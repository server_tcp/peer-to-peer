#ifndef __DEBUG_H__
#define __DEBUG_H__

/**
 * \brief main file contain all code definition for debug
 *
 * \file debug.h
 * \author Gautier Levesque
 * \date 13-01-2024
 */

/**
 * \brief print a message with a timestamp
 *
 * \param msg array of char to print
 */
void print_msg(const char *msg);

/**
 * \brief print a message with a timestamp and an IP
 *
 * \param msg array of char to print
 * \param ip array define a ip address
 */
void print_msg_with_ip(const char *msg, const char *ip);

#endif