#!/bin/bash

file_path="ip_add_list.txt"

> "$file_path"

arp-scan --localnet | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" >> "$file_path"

echo "Les adresses IP ont été enregistrées dans $file_path"
