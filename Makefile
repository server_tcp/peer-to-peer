CC=gcc
CFLAGS=-Wall -Wextra -pedantic -pthread -lssl -lcrypto

SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
EXEC=test_server

all: 
	gcc *.c -Wall -Wextra -pedantic -pthread -lssl -lcrypto -o $(EXEC)

doc:
	doxygen $(DOXYGEN_CONFIG_FILE)

clean:
	rm -f $(OBJS) $(EXEC)
