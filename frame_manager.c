/**
 * \brief main file contain all code implementation for frame parser
 *
 * \file frame_parser.c
 * \author Gautier Levesque
 * \date 12-01-2024
 */

#include "server_tcp.h"

/**
 * \brief define first octet of a header message
 */
#define START_MSG 0x01

bool parse_frame(Open_socket *socket, const uint32_t my_id)
{
    if (socket->stop == true)
        return true;

    if (socket == NULL && (socket->buffer_receive == NULL || socket->buffer_idx_receive == 0))
    {
        print_msg("Buffer is NULL or size is 0 so no frame to parse");
        return true;
    }

    /*printf("\r\n Start of message \r\n\r\n");
    for (uint16_t i = 0; i < socket->buffer_idx_receive; ++i)
    {
        printf(" 0x%02x", socket->buffer_receive[i]);
    }
    printf("\r\n End of message \r\n\r\n");*/

    uint16_t i = 0;
    bool first_frame = false;
    if (socket->frame_analyser.data_read == false)
    {
        first_frame = true;
        // search for message beging header
        for (; i < socket->buffer_idx_receive; ++i)
        {
            if (socket->buffer_receive[i] == START_MSG)
            {
                memcpy(&socket->frame_analyser.msg_type, &socket->buffer_receive[i + 1], 1);
                memcpy(&socket->frame_analyser.msg_receive_size, &socket->buffer_receive[i + 6], 4);
                memcpy(&socket->frame_analyser.id_sender, &socket->buffer_receive[i + 2], 4);
                uint8_t confg_msg = socket->buffer_receive[i + 10];
                socket->frame_analyser.is_crc = confg_msg & 0x01;
                socket->frame_analyser.data_read = true;
                break;
            }
        }
    }

    // no header so no message
    if (socket->frame_analyser.data_read == false)
    {
        print_msg("No header found");
        return false;
    }

    switch (socket->frame_analyser.msg_type)
    {
    case MSG_PING:
        print_msg("Ping message received");
        if (socket->is_wait_ping_ack == true)
        {
            socket->is_wait_ping_ack = false;
            send_ack(my_id, socket, MSG_PING);
        }
        socket->frame_analyser.data_read = false;
        break;
    case MSG_ID:
        print_msg("Id message received");
        if (first_frame == true)
        {
            if (socket->is_wait_for_id == true)
            {
                if (socket->buffer_receive[11] == 0x01)
                {
                    socket->is_verified = is_valid_client(socket->frame_analyser.id_sender);
                    if (socket->is_verified == true)
                        memcpy(&socket->id_client, &socket->frame_analyser.id_sender, sizeof(uint32_t));
                }
            }
        }
        else
        {
            if (socket->is_wait_for_id == true)
            {
                if (socket->buffer_receive[0] == 0x01)
                {
                    socket->is_verified = is_valid_client(socket->frame_analyser.id_sender);
                    if (socket->is_verified == true)
                        memcpy(&socket->id_client, socket->buffer_receive, sizeof(uint32_t));
                }
            }
        }
        socket->frame_analyser.data_read = false;
        break;
    case MSG_BYE:
        socket->stop = true;
        socket->frame_analyser.data_read = false;
        break;
    default:
        print_msg("Unknown message type");
        socket->frame_analyser.data_read = false;
        return false;
    }

    return true;
}

void send_ack(const uint32_t id, Open_socket *soc, Type_msg_enum type_msg)
{
    uint8_t msg[11];
    uint32_t size = 1;
    memset(msg, 0, 11);

    msg[0] = START_MSG;
    msg[1] = type_msg;
    memcpy(msg + 6, &size, 4);
    memcpy(msg + 2, &id, 4);

    write_msg(msg, 11, soc);

    uint8_t msg_2 = 0x01;
    write_msg(&msg_2, 1, soc);
}

void send_ping(Open_socket *soc, uint32_t id)
{
    print_msg("Send ping");
    uint8_t msg[11];
    uint16_t size = 0;
    memset(msg, 0, 11);

    msg[0] = START_MSG;
    msg[1] = MSG_PING;
    memcpy(msg + 6, &size, 4);
    memcpy(msg + 2, &id, 4);

    write_msg(msg, 11, soc);
}

void send_bye(const uint32_t id, Open_socket *soc)
{
    uint8_t msg[11];
    uint32_t size = 0;
    memset(msg, 0, 11);

    msg[0] = START_MSG;
    msg[1] = MSG_BYE;
    memcpy(msg + 6, &size, 4);
    memcpy(msg + 2, &id, 4);

    write_msg(msg, 11, soc);
}

void send_who_are_you(const uint32_t id, Open_socket *soc)
{
    uint8_t msg[11];
    uint32_t size = 0;
    memset(msg, 0, 11);

    msg[0] = START_MSG;
    msg[1] = MSG_ID;
    memcpy(msg + 6, &size, 4);
    memcpy(msg + 2, &id, 4);

    write_msg(msg, 11, soc);
}

void write_msg(uint8_t *buffer, const uint16_t size, Open_socket *tmp_soc)
{
    if (tmp_soc->buffer_idx_send + size >= BUFFER_SEND_SIZE)
    {
        print_msg("Buffer send full");
        return;
    }

    memcpy(tmp_soc->buffer_send + tmp_soc->buffer_idx_send, buffer, size);
    tmp_soc->buffer_idx_send += size;
}