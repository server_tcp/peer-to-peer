#include "debug.h"

/**
 * \brief main file contain all code implementation for debug
 *
 * \file debug.c
 * \author Gautier Levesque
 * \date 13-01-2024
 */

#include <stdio.h>
#include <time.h>

void print_msg(const char *msg)
{
    time_t timestamp;
    time(&timestamp);
    struct tm *local_time = localtime(&timestamp);

    // print msg with timestamp
    printf("%02d-%02d-%04d %02d:%02d:%02d: %s\r\n",
           local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,
           local_time->tm_hour, local_time->tm_min, local_time->tm_sec, msg);
}

void print_msg_with_ip(const char *msg, const char *ip)
{
    time_t timestamp;
    time(&timestamp);
    struct tm *local_time = localtime(&timestamp);

    // print msg with timestamp
    printf("%02d-%02d-%04d %02d:%02d:%02d from %s: %s\r\n",
           local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,
           local_time->tm_hour, local_time->tm_min, local_time->tm_sec, ip, msg);
}