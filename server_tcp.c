/**
 * \brief main file contain all code implementation for a TCP server
 *
 * \file server_tcp.c
 * \author Gautier Levesque
 * \date 12-12-2023
 */

#include "server_tcp.h"

typedef struct
{
    uint8_t idx;
} Thread_args;

static char my_ip[INET6_ADDRSTRLEN];
static uint8_t openned_socket = 0;
static Open_socket sockets_array[MAX_SOCKET];
static uint32_t id = 0x000000;
static int port = 0;
static SSL_CTX *ssl_ctx = NULL;

void *thread_socket_manager(void *args)
{
    Thread_args *args_struct = (Thread_args *)args;

    // get socket
    Open_socket *private_socket = &sockets_array[args_struct->idx];
    time_t time_since_last_ping = time(NULL);
    uint8_t time_before_timeout = 30;
    uint8_t time_before_ping = 10 + rand() % 20;

    struct pollfd fds[2];
    fds[0].fd = private_socket->socket;
    fds[0].events = POLLIN;
    fds[1].fd = private_socket->socket;
    fds[1].events = POLLOUT;
    int timeout = 1000;

    int check_poll;

    while (private_socket->stop == false)
    {
        check_poll = poll(fds, 2, timeout);

        if (check_poll == -1)
        {
            print_msg("Poll error");
            continue;
        }

        if (private_socket->buffer_idx_send > 0 && fds[1].revents & POLLOUT)
        {
            print_msg("Send message");
            ssize_t bytes_send = SSL_write(private_socket->ssl, private_socket->buffer_send, private_socket->buffer_idx_send);
            if (bytes_send == -1)
            {
                print_msg("Error write");
                private_socket->stop = true;
            }
            private_socket->buffer_idx_send -= bytes_send;
        }

        if (fds[0].revents & POLLIN)
        {
            ssize_t bytes_read = SSL_read(private_socket->ssl, private_socket->buffer_receive, BUFFER_SIZE);
            if (bytes_read == -1)
            {
                print_msg("Error read");
                private_socket->stop = true;
            }
            else if (bytes_read > 0)
            {
                print_msg("Message receive");
                private_socket->buffer_idx_receive += bytes_read;
                if (parse_frame(private_socket, id) == true)
                {
                    private_socket->last_communication = time(NULL);
                }
                private_socket->buffer_idx_receive = 0;
            }
        }

        if (time(NULL) - private_socket->last_communication > time_before_timeout)
            private_socket->stop = true;
        else if (time(NULL) - private_socket->last_communication > time_before_ping)
        {
            if (time(NULL) - time_since_last_ping > 1)
            {
                private_socket->is_wait_ping_ack = true;
                send_ping(private_socket, id);
                time_since_last_ping = time(NULL);
            }
        }
    }

    SSL_shutdown(private_socket->ssl);
    SSL_free(private_socket->ssl);
    SSL_CTX_free(private_socket->ctx);
    close_socket(private_socket);
    return NULL;
}

void *thread_accept_socketV4(void *args)
{
    int *socket_tcp = (int *)args;
    while (1)
    {
        print_msg("Server waiting for a new client IPV4");
        // attempt place for a client
        while (openned_socket >= MAX_SOCKET)
            ;

        uint8_t i = 0;
        // searching a place in socket array
        for (; i < MAX_SOCKET; ++i)
        {
            // place found
            if (sockets_array[i].is_use == false)
                break;
        }
        // accept a client
        struct sockaddr_in client_address;
        socklen_t client_address_len = sizeof(client_address);
        int client_socket = accept(*socket_tcp, (struct sockaddr *)&client_address, &client_address_len);

        // connection failed
        if (client_socket == -1)
        {
            print_msg("Server trying to connect a client socket, but failed");
            continue;
        }

        sockets_array[i].ssl = SSL_new(ssl_ctx);
        SSL_set_fd(sockets_array[i].ssl, client_socket);

        if (SSL_accept(sockets_array[i].ssl) <= 0)
        {
            print_msg("Error trying to get ssl for client socket");
            close(client_socket);
            continue;
        }

        // No bloquant socket
        int flags = fcntl(client_socket, F_GETFL, 0);
        if (flags == -1)
        {
            print_msg("Error get client flag");
            close(client_socket);
            continue;
        }

        if (fcntl(client_socket, F_SETFL, flags | O_NONBLOCK) == -1)
        {
            print_msg("Error when set no blocking socket");
            close(client_socket);
            continue;
        }

        if (openned_socket < MAX_SOCKET)
        {
            if (connect_socket_server(&client_socket, &client_address, i) == false)
                close(client_socket);
        }
        else
            close(client_socket);
    }

    return NULL;
}

bool connect_socket_server(int *soc, void *client_addr, uint8_t place_idx)
{
    Thread_args *args_struct = malloc(sizeof(Thread_args));
    if (args_struct == NULL)
    {
        perror("Malloc : ");
        return false;
    }

    args_struct->idx = place_idx;
    uint8_t i = place_idx;
    // init array case
    sockets_array[i].is_use = true;
    sockets_array[i].socket = *soc;
    sockets_array[i].id_client = 0;
    sockets_array[i].buffer_idx_receive = 0;
    sockets_array[i].buffer_idx_send = 0;
    sockets_array[i].last_communication = time(NULL);
    sockets_array[i].stop = false;
    sockets_array[i].is_verified = false;

    struct sockaddr_in *addr_V4 = (struct sockaddr_in *)client_addr;
    inet_ntop(AF_INET, &addr_V4->sin_addr, sockets_array[i].client_ip, sizeof(sockets_array[i].client_ip));

    memset(sockets_array[i].buffer_send, 0, sizeof(uint8_t) * BUFFER_SIZE);
    memset(sockets_array[i].buffer_receive, 0, sizeof(uint8_t) * BUFFER_SIZE);

    print_msg_with_ip("Connexion open", sockets_array[i].client_ip);

    // starting thread manager
    pthread_t thread_socket;

    if (pthread_create(&thread_socket, NULL, thread_socket_manager, (void *)args_struct) != 0)
    {
        sockets_array[i].stop = true;
        sockets_array[i].is_use = false;
        print_msg_with_ip("Error when thread trying to init thread manager", sockets_array[i].client_ip);
        return false;
    }

    openned_socket += 1;

    sockets_array[i].is_wait_for_id = true;
    send_who_are_you(id, &sockets_array[i]);

    sleep(5);

    free(args_struct);
    args_struct = NULL;

    // client verification
    time_t past_time = time(NULL);
    time_t current_time = time(NULL);
    while (sockets_array[i].is_verified == false && current_time - past_time < 60)
    {
        current_time = time(NULL);
    }

    if (sockets_array[i].is_verified == false)
    {
        print_msg_with_ip("Connexion invalidate", sockets_array[i].client_ip);
        sockets_array[i].stop = true;
        sockets_array[i].is_use = false;
        return false;
    }
    else
        print_msg_with_ip("Connexion validate", sockets_array[i].client_ip);

    return true;
}

int8_t init_connection(int *socket_tcp_V4)
{
    print_msg("Starting to initialize server");

    memset(sockets_array, 0, sizeof(Open_socket) * MAX_SOCKET);

    *socket_tcp_V4 = socket(AF_INET, SOCK_STREAM, 0);
    if (*socket_tcp_V4 == -1)
    {
        print_msg("Error while creating server socket V4");
        return -1;
    }

    memset(my_ip, 0, sizeof(char) * INET_ADDRSTRLEN);

    // recuperation de l'adresse ip local du PC
    FILE *file = fopen("server_config.txt", "r");
    if (file == NULL)
    {
        close(*socket_tcp_V4);
        print_msg("Error when opening config file");
        return -1;
    }

    fscanf(file, "Local IPV4 addr : %s\r\n", my_ip);
    fscanf(file, "Port : %d", &port);

    fclose(file);
    file = NULL;

    // remove CIDR
    memset(my_ip + strlen(my_ip) - 3, 0, 3);

    char str[119];
    memset(str, 0, sizeof(str));
    snprintf(str, sizeof(str), "Local IPV4 addr : %s and port : %d", my_ip, port);
    print_msg(str);

    struct sockaddr_in server_address_V4;

    memset(&server_address_V4, 0, sizeof(server_address_V4));

    server_address_V4.sin_family = AF_INET;
    server_address_V4.sin_addr.s_addr = INADDR_ANY;
    server_address_V4.sin_port = htons(port);

    if (bind(*socket_tcp_V4, (struct sockaddr *)&server_address_V4, sizeof(server_address_V4)) == -1)
    {
        print_msg("Error when binding socket");
        close(*socket_tcp_V4);
        return -1;
    }

    if (listen(*socket_tcp_V4, 10) == -1)
    {
        print_msg("Error while trying to listen");
        close(*socket_tcp_V4);
        return -1;
    }

    return 0;
}

void close_socket(Open_socket *soc)
{
    soc->is_use = false;
    soc->stop = true;
    openned_socket -= 1;
    if (close(soc->socket) == -1)
        print_msg_with_ip("Unable to close socket", soc->client_ip);
    else
        print_msg_with_ip("Socket close", soc->client_ip);
}

bool is_valid_client(const uint32_t id)
{
    for (uint8_t i = 0; i < openned_socket; ++i)
    {
        if (sockets_array[i].is_use)
        {
            if (sockets_array[i].id_client == id)
            {
                return true;
            }
        }
    }

    return false;
}

void search_new_ip(void)
{
    if (openned_socket >= MAX_SOCKET)
        return;

    const char *script_path_broadcast_ip = "./script_sh/broadcast_ip_finder.sh";
    int result = system(script_path_broadcast_ip);
    if (result == -1)
    {
        print_msg("Error while trying to find all IP addr on network");
        return;
    }

    // starting to connect to all IP adresse found
    print_msg("Starting to try to connect to all IP addr found");

    FILE *file_ip = fopen("ip_add_list.txt", "r");
    if (file_ip == NULL)
    {
        print_msg("Error when opening ip file");
        return;
    }

    char ip_name[INET6_ADDRSTRLEN];
    memset(ip_name, 0, INET6_ADDRSTRLEN);
    while (fgets(ip_name, INET6_ADDRSTRLEN, file_ip) != NULL)
    {
        // printf("IP : %s length : %d\r\n", ip_name, strlen(ip_name));

        uint8_t length_ip = strlen(ip_name);
        for (uint8_t i = 0; i < length_ip; ++i)
        {
            if (ip_name[i] == '\r' || ip_name[i] == '\n' || ip_name[i] == '\\' || ip_name[i] == '/')
                ip_name[i] = '\0';
        }
        bool already_open = false;

        if (memcmp(ip_name, my_ip, INET6_ADDRSTRLEN) == 0)
            continue;

        // verification de pas deja ouverte avec cette IP
        uint8_t i = 0;
        for (i = 0; i < MAX_SOCKET; ++i)
        {
            if (sockets_array[i].is_use == true && memcmp(ip_name, sockets_array[i].client_ip, INET6_ADDRSTRLEN) == 0)
            {
                already_open = true;
                break;
            }
        }

        if (already_open == true)
            continue;

        i = 0;
        // searching a place in socket array
        for (; i < MAX_SOCKET; ++i)
        {
            // place found
            if (sockets_array[i].is_use == false)
                break;
        }

        if (connect_socket_client(ip_name, i) == false)
            print_msg("Failed to connect IP");

        memset(ip_name, 0, INET6_ADDRSTRLEN);
    }

    fclose(file_ip);
    file_ip = NULL;
}

bool connect_socket_client(const char *ip, uint8_t place_id)
{
    if (openned_socket >= MAX_SOCKET)
        return false;

    Thread_args *args_struct = malloc(sizeof(Thread_args));
    if (args_struct == NULL)
    {
        perror("Malloc : ");
        return false;
    }

    uint8_t i = place_id;
    args_struct->idx = i;

    if(create_ssl_context_for_client(&sockets_array[i].ctx) < 0)
    {
        print_msg("Error when trying to create ssl context for client");
        return false;
    }

    if(configure_client_context(&sockets_array[i].ctx) < 0)
    {
        print_msg("Error when trying to configure ssl client context");
        return false;
    }

    Open_socket *client_socket = &sockets_array[i];

    struct sockaddr_in server_address;
    client_socket->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket->socket == -1)
    {
        print_msg("Error when creating client socket");
        return false;
    }

    client_socket->is_use = true;
    client_socket->id_client = 0;
    client_socket->buffer_idx_receive = 0;
    client_socket->buffer_idx_send = 0;
    client_socket->last_communication = time(NULL);
    client_socket->stop = false;
    client_socket->is_verified = true;

    memcpy(client_socket->client_ip, ip, INET6_ADDRSTRLEN);
    memset(client_socket->buffer_send, 0, sizeof(uint8_t) * BUFFER_SIZE);
    memset(client_socket->buffer_receive, 0, sizeof(uint8_t) * BUFFER_SIZE);

    print_msg_with_ip("Connexion open", client_socket->client_ip);

    // No bloquant socket
    int flags = fcntl(client_socket->socket, F_GETFL, 0);
    if (flags == -1)
    {
        print_msg("Error get client flag");
        close(client_socket->socket);
        return false;
    }

    if (fcntl(client_socket->socket, F_SETFL, flags | O_NONBLOCK) == -1)
    {
        print_msg("Error when set no blocking socket");
        close(client_socket->socket);
        return false;
    }

    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(port);
    if (inet_pton(AF_INET, ip, &server_address.sin_addr) <= 0)
    {
        print_msg("Error when convert IP addr");
        close(client_socket->socket);
        return false;
    }

    if (connect(client_socket->socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        if (errno != EINPROGRESS)
        {
            print_msg("Error when connecting to server");
            close(client_socket->socket);
            return false;
        }
    }

    fd_set writeSet;
    FD_ZERO(&writeSet);
    FD_SET(client_socket->socket, &writeSet);

    struct timeval timeout;
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;
    int ready = select(client_socket->socket + 1, NULL, &writeSet, NULL, &timeout);
    if (ready == -1)
    {
        print_msg("Error when calling select");
        close(client_socket->socket);
        return false;
    }
    else if (ready == 0)
    {
        print_msg("Connexion tiemout");
        close(client_socket->socket);
        return false;
    }

    client_socket->ssl = SSL_new(client_socket->ctx);
    SSL_set_fd(client_socket->ssl, client_socket->socket);
    SSL_set_tlsext_host_name(client_socket->ssl, client_socket->client_ip);
    SSL_set1_host(client_socket->ssl, client_socket->client_ip);

    if(SSL_connect(client_socket->ssl) != 1)
    {
        print_msg("Error when trying connect ssl socket");
        close(client_socket->socket);
        return false;
    }

    // starting thread manager
    pthread_t thread_socket;

    if (pthread_create(&thread_socket, NULL, thread_socket_manager, (void *)args_struct) != 0)
    {
        sockets_array[i].stop = true;
        sockets_array[i].is_use = false;
        print_msg_with_ip("Error when thread trying to init thread manager", sockets_array[i].client_ip);
        return false;
    }

    free(args_struct);
    args_struct = NULL;

    openned_socket += 1;

    print_msg("Client connection success to server");

    return true;
}

/**
 * \brief main function
 *
 * \return 0 if success or error code
 */
int main(void)
{
    srand(time(NULL));

    signal(SIGPIPE, SIG_IGN);
    const char *script_path_ip_finder = "./script_sh/local_ip_finder.sh";
    int result = system(script_path_ip_finder);
    if (result == -1)
    {
        print_msg("Error while trying to find local IP addr");
        return -1;
    }

    int socket_TCPV4 = 0;

    if (init_connection(&socket_TCPV4) < 0)
    {
        print_msg("Error when starting connection");
        return -1;
    }

    ssl_ctx = NULL;

    // generation du context ssl pour le serveur
    if (create_ssl_context_for_server(&ssl_ctx) < 0)
    {
        print_msg("Error when trying to init ssl context");
        return -1;
    }

    if (configure_server_context(&ssl_ctx) < 0)
    {
        print_msg("Error when trying to configure ssl context");
        return -1;
    }


    search_new_ip();

    print_msg("Connection initialization success");

    pthread_t thread_socket_manager_V4;
    if (pthread_create(&thread_socket_manager_V4, NULL, thread_accept_socketV4, (void *)&socket_TCPV4) != 0)
    {
        print_msg("Error when thread init for accept socket");
        return -1;
    }

    if (pthread_join(thread_socket_manager_V4, NULL) != 0)
    {
        print_msg("Error when waiting thread");
        return -1;
    }

    SSL_CTX_free(ssl_ctx);
    close(socket_TCPV4);

    return 0;
}
