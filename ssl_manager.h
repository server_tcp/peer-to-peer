#ifndef __SSL_MANAGER_H__
#define __SSL_MANAGER_H__

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <signal.h>

int configure_server_context(SSL_CTX **ctx);
int configure_client_context(SSL_CTX **ctx);
int create_ssl_context_for_server(SSL_CTX **context);
int create_ssl_context_for_client(SSL_CTX **context);

#endif