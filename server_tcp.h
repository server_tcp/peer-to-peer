#ifndef __SERVER_TCP_H__
#define __SERVER_TCP_H__

/**
 * \brief main file contain all code definition for a TCP server
 *
 * \file server_tcp.h
 * \author Gautier Levesque
 * \date 29-12-2023
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdint.h>
#include <time.h>
#include <semaphore.h>
#include <poll.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/time.h>
#include "ssl_manager.h"

#include "debug.h"

/**
 * \brief use to know how many socket max we will open at same time
 */
#define MAX_SOCKET 10
/**
 * \brief use to know buffer max size in receive or transmit
 */
#define BUFFER_SIZE 128
/**
 * \brief define how many message can me get in buffer send
 */
#define BUFFER_SEND_SIZE 20

/**
 * \brief use to enumerate all type of message we can receive
 */
typedef enum
{
    MSG_VIDEO = 0xA0,
    MSG_PING = 0xA1,
    MSG_ORDER = 0xA2,
    MSG_ID = 0xA3,
    MSG_BYE = 0xA4,
} Type_msg_enum;

/**
 * \brief use to analyse a frame
 */
typedef struct
{
    bool data_read;            /**<data_read> use to know if we have to read data*/
    uint32_t msg_receive_size; /**<msg_receive_size> use to know how many octet we have to read in data before end of message*/
    uint32_t id_sender;        /**<is_sender> id of frame sender*/
    bool is_crc;               /**<is_crc> if message contain a crc*/
    Type_msg_enum msg_type;    /**<msg_type> type of message*/
} Frame_analyser;

/**
 * \brief represent an openned socket
 */
typedef struct
{
    char client_ip[INET6_ADDRSTRLEN];    /**<client_ip> ip address of client*/
    uint32_t id_client;                  /**<id_client> id of client*/
    int socket;                          /**<socket> open socket descriptor*/
    bool is_use;                         /**<is_use> if socket is currently use*/
    uint8_t buffer_receive[BUFFER_SIZE]; /**<buffer_receive> buffer of socket receive*/
    uint8_t buffer_send[BUFFER_SIZE];    /**<buffer_send> buffer of what we need to send to socket*/
    uint16_t buffer_idx_receive;         /**<buffer_idx_receive> how many octet are in buffer_receive*/
    uint16_t buffer_idx_send;            /**<buffer_idx_send> current place in buffer_send*/
    time_t last_communication;           /**<last_communication> time since last communication with socket*/
    bool stop;                           /**<stop> if we need to close socket*/
    Frame_analyser frame_analyser;       /**<frame_analyser> use to analyse a frame*/
    bool is_verified;                    /**<is_verified> use to know if this socket is a accepted device*/
    bool is_wait_for_id;                 /**<is_wait_for_id> if this socket accept a connection and wait for validate it*/
    bool is_wait_ping_ack;               /**<is_wait_ping_ack> if this socket wait for a ping ack*/
    SSL *ssl;
    SSL_CTX *ctx;
} Open_socket;

/**
 * \brief a thread that launch, role is to accept new socket from IPV4
 *
 * \param args pointer of argument passed with this function
 *
 * \return NULL if success
 */
void *thread_accept_socketV4(void *args);

/**
 * \brief use to manager an openned socket
 *
 * \param args pointer of argument passed with this function
 *
 * \param return NULL if success
 */
void *thread_socket_manager(void *args);

/**
 * \brief use to connect a socket server case
 *
 * \param soc pointer to connected socket
 * \param client_addr pointer to client addr
 * \param place_idx idx of sockets_array where socket is located
 *
 * \return true if success, false instead
 */
bool connect_socket_server(int *soc, void *client_addr, uint8_t place_idx);

/**
 * \brief use to connect a socket client case
 *
 * \param ip ip to connect
 * \param place_id place on socket_array who will be use for socket
 *
 * \return true if success, false instead
 */
bool connect_socket_client(const char *ip, uint8_t place_id);

/**
 * \brief use to launch server
 *
 * \param socket_tcp_V4 pointer of server socket with V4 IP
 *
 * \return error code or 0 if success
 */
int8_t init_connection(int *socket_tcp_V4);

/**
 * \brief use to parse a frame receive by a socket
 *
 * \param socket pointer to current socket
 * \param id my id
 *
 * \return true if frame is okay
 */
bool parse_frame(Open_socket *socket, const uint32_t my_id);

/**
 * \brief ack sender
 *
 * \param id id of sender
 * \param soc socket to send message
 * \param type_msg type of message
 */
void send_ack(const uint32_t id, Open_socket *soc, Type_msg_enum type_msg);

/**
 * \brief when we need to close properly a socket
 *
 * \param id id of sender
 * \param soc pointer to current socket
 */
void send_bye(const uint32_t id, Open_socket *soc);

/**
 * \brief use to validat a connection
 *
 * \param id id of sender
 * \param soc pointer to current socket
 */
void send_who_are_you(const uint32_t id, Open_socket *soc);

/**
 * \brief use to close a socket
 *
 * \param soc pointer to socket to close
 */
void close_socket(Open_socket *soc);

/**
 * \brief use to check if a client id is correct
 *
 * \param id current id of client
 *
 * \return false if incorrect, true instead
 */
bool is_valid_client(const uint32_t id);

/**
 * \brief use to send a ping to server
 *
 * \param soc pointer to current socket
 * \param id my id
 */
void send_ping(Open_socket *soc, uint32_t id);

/**
 * \brief wrapper to write message in buffer sender
 *
 * \param buffer buffer to send
 * \param size size of buffer
 * \param tmp_soc socket to send
 */
void write_msg(uint8_t *buffer, const uint16_t size, Open_socket *tmp_soc);

#endif