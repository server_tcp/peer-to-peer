#include "ssl_manager.h"

int configure_server_context(SSL_CTX **ctx)
{
    if (SSL_CTX_use_certificate_chain_file(*ctx, "./CertServeur/cert-server.pem") < 0)
        return -1;

    if (SSL_CTX_use_PrivateKey_file(*ctx, "./CertServeur/cert-key.pem", SSL_FILETYPE_PEM) < 0)
        return -1;

    return 0;
}

int configure_client_context(SSL_CTX **ctx)
{  
    SSL_CTX_set_verify(*ctx, SSL_VERIFY_PEER, NULL);

    if (!SSL_CTX_load_verify_locations(*ctx, "./CA/ca-cert.pem", NULL))
        return -1;

    return 0;
}

int create_ssl_context_for_client(SSL_CTX **context)
{
    const SSL_METHOD *method;

    method = TLS_client_method();
    *context = SSL_CTX_new(method);
    if (*context == NULL)
        return -1;

    return 0;
}

int create_ssl_context_for_server(SSL_CTX **context)
{
    const SSL_METHOD *method;

    method = TLS_server_method();

    *context = SSL_CTX_new(method);
    if (*context == NULL)
        return -1;

    return 0;
}