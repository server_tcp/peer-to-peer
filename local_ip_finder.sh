#!/bin/bash

if command -v ip &> /dev/null; then
    ipv4_address=$(ip -o -4 addr show | awk '/^ *[0-9]+:/ {sub(/:/,"",$2); iface=$2} $2 ~ /^wl/ {print $4; exit}' | grep -v '^fe80' | head -n 1)
else
    echo "IP unix command not found, install it before launching this program"
    exit -1
fi

file_path="server_config.txt"
port="65430"

if [ "$ipv4_address" = "" ]; then
    echo "No network interface with 'wl' found."
else
    echo "Local IPV4 addr : $ipv4_address" > "$file_path"
    echo "Port : $port" >> "$file_path"
    echo "Local IPV4 addr and port are registered in $file_path."
fi
