cd CA
openssl genrsa -aes256 -out ca-key.pem 4096
openssl req -new -x509 -sha256 -days 365 -key ca-key.pem -out ca-cert.pem

cd ../CertServeur
openssl genrsa -out cert-key.pem 4096
openssl req -new -sha256 -subj "/CN=runLittleBoy" -key cert-key.pem -out cert-query.csr
echo subjectAltName=IP:$1 > extfile.cnf
openssl x509 -req -sha256 -days 365 -in cert-query.csr -CA ../CA/ca-cert.pem -CAkey ../CA/ca-key.pem -out cert-server.pem -extfile extfile.cnf -CAcreateserial
